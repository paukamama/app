const percentageToHsl = (percentage, hue0 = 135, hue1 = 350) => {
    let hue = ((percentage / 100) * (hue1 - hue0)) + hue0;
    return 'hsl(' + hue + ', 100%, 50%)';
};

export default percentageToHsl;