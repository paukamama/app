class Route {
    constructor(src, dst) {
        this.src = src;
        this.dst = dst;
    }
}

export default Route;