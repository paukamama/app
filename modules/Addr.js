class Addr {
    constructor(addr, coord) {
        this.addr = addr;
        this.coord = coord;
    }

    coordinatesEmpty() {
        return Object.getOwnPropertyNames(this.coord).length === 0;
    }
}

export default Addr;