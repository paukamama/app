import React, {useState, useEffect} from 'react';
import {
    Button,
    Card,
    Container,
    Content,
    Form,
    Text
} from 'native-base';
import AddrField from '../components/Accueil/AddrField';
import SavedRoutes from '../components/Accueil/SavedRoutes';
import Addr from '../modules/Addr'
import handleAddressLookup from '../services/handleAddressLookup'
import savedRoutesDB from '../services/savedRoutesDB'

const Accueil = (props) => {
    const [src, setSrc] = useState(new Addr("", {}));
    const [srcSuggestions, setSrcSuggestions] = useState([]);
    const [srcSelected, setSrcSelected] = useState(false);
    const [dst, setDst] = useState(new Addr("", {}));
    const [dstSuggestions, setDstSuggestions] = useState([]);
    const [dstSelected, setDstSelected] = useState(false);

    const updateSuggestions = (getter, suggestionsSetter) => {
        return () => {
            const addr = getter.addr;
            if (addr.length > 0)
                return handleAddressLookup(addr)
                    .then(suggestions => suggestionsSetter(suggestions));
        }
    }
    const updateSrcSuggestions = updateSuggestions(src, setSrcSuggestions);
    const updateDstSuggestions = updateSuggestions(dst, setDstSuggestions);
    useEffect(() => {
        if(!srcSelected) 
            updateSrcSuggestions()
    }, [src]);
    useEffect(() => {
        if(!dstSelected) 
            updateDstSuggestions()
    }, [dst]);

    const handleQuery = () => {
        const {navigate} = props.navigation;
        let srcPromise, dstPromise;

        if (src.coordinatesEmpty())
            srcPromise =  updateSrcSuggestions()
                .then(() => srcSuggestions[0].coord);
        else
            srcPromise = Promise.resolve(src.coord);

        if (dst.coordinatesEmpty())
            dstPromise = updateDstSuggestions()
                .then(() => dstSuggestions[0].coord);
        else
            dstPromise = Promise.resolve(dst.coord);

        Promise.all([srcPromise, dstPromise])
            .then(values => {
                navigate('Propositions', {
                    srcAddress: values[0],
                    dstAddress: values[1]
                })
            });
    }

    return (
        <Container>
            <Content>
                <Card transparent>
                    <Form>
                        <AddrField
                            placeholder="D'où partez-vous ?"
                            value={src.addr}
                            onChangeText={addr => {
                                setSrcSelected(false);
                                setSrc(new Addr(addr, {}))
                            }}
                            suggestionsArray={srcSuggestions}
                            onSuggestionsUpdate={(addr) => {
                                setSrc(addr);
                                setSrcSelected(true);
                                setSrcSuggestions([]);
                            }}
                        />
                        <AddrField
                            placeholder="Où allez-vous ?"
                            value={dst.addr}
                            onChangeText={addr => {
                                setDstSelected(false);
                                setDst(new Addr(addr, {}));
                            }}
                            suggestionsArray={dstSuggestions}
                            onSuggestionsUpdate={(addr) => {
                                setDst(addr);
                                setDstSelected(true);
                                setDstSuggestions([]);
                            }}
                        />
                    </Form>
                </Card>

                <Button
                    block
                    style={{backgroundColor: '#7E7E7E'}}
                    onPress={() => handleQuery()}>
                    <Text>Rechercher</Text>
                </Button>
                <SavedRoutes savedRoutes={savedRoutesDB} navigate={props.navigation.navigate}/>
            </Content>
        </Container>
    );
}

export default Accueil;
