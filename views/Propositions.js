import React, {useState, useEffect} from 'react';
import {Spinner, Button, Card, CardItem, Container, Content, Text} from 'native-base';
import PathInfoCard from '../components/Propositions/PathInfoCard'
import PropositionMap from '../components/Propositions/PropositionMap'
import percentageToHsl from '../modules/percentageToHsl'
import PathSwitcher from '../components/Propositions/PathSwitcher';
import handlePathQuery from '../services/handlePathQuery'

const Propositions = (props) => {
    const [selectedPathIndex, setSelectedPathIndex] = useState(0);
    const [fastestPathIndex, setFastestPathIndex] = useState(0);
    const [cleanestPathIndex, setCleanestPathIndex] = useState(0);
    const [paths, setPaths] = useState([]);

    useEffect(() => {
        handlePathQuery(getSrc(), getDst())
            .then(data => {
                setSelectedPathIndex(0);
                setFastestPathIndex(getFastestPathIndex(data.paths));
                setCleanestPathIndex(getCleanestPathIndex(data.paths));
                setPaths(data.paths);
            }).catch((error) => {
                alert(error.message);
            });
    }, []);

    const getSrc = () => props.navigation.getParam('srcAddress');

    const getDst = () => props.navigation.getParam('dstAddress');

    const getCleanestPathIndex = (paths) => {
        let cleanestPathIndex = 0;
        let minPollution = 100;
        let i, currentPollution;
        for (i = 0; i < paths.length; i++) {
            currentPollution = paths[i]["pollution"];
            if (currentPollution < minPollution) {
                minPollution = currentPollution;
                cleanestPathIndex = i
            }
        }
        return cleanestPathIndex;
    }

    const getFastestPathIndex = (paths) => {
        let fastestPathIndex = 0;
        let minDuration = paths[0]["duration"];
        let j, currentDuration;
        for (j = 0; j < paths.length; j++) {
            currentDuration = paths[j]["duration"];
            if (currentDuration < minDuration) {
                minDuration = currentDuration;
                fastestPathIndex = j;
            }
        }
        return fastestPathIndex;
    }

    const isFastestPathSelected = () => (selectedPathIndex === fastestPathIndex);

    const isCleanestPathSelected = () => (selectedPathIndex === cleanestPathIndex);

    const getColorFromPollution = (pollution, index) => {
        if (index === selectedPathIndex && pollution >= 0 && pollution <= 100)
            return percentageToHsl(pollution);
        return "#7b7b7b";
    };

    const getLineWeight = (index) => (index === selectedPathIndex) ? 6 : 2

    const {navigate} = props.navigation;

    return (
        <Container>
            {(paths.length > 0) ? (
                <React.Fragment>
                    <Content>
                        <PropositionMap
                            paths={paths}
                            getColorFromPollution={getColorFromPollution}
                            getLineWeight={getLineWeight}
                            setSelectedPathIndex={setSelectedPathIndex}
                            src={getSrc()}
                            dst={getDst()} 
                        />
                    </Content>
                    <Card transparent>
                        <CardItem>
                            <PathSwitcher 
                                isFastestPathSelected={isFastestPathSelected()}
                                isCleanestPathSelected={isCleanestPathSelected()}
                                setSelectedPathIndex={setSelectedPathIndex}
                                fastestPathIndex={fastestPathIndex}
                                cleanestPathIndex={cleanestPathIndex}
                            />
                        </CardItem>
                        <CardItem>
                            <PathInfoCard
                                path={paths[selectedPathIndex]}
                                color={getColorFromPollution(paths[selectedPathIndex]["pollution"], selectedPathIndex)}
                                mutedColor='#434343'
                            />
                            <Button
                                rounded
                                style={{backgroundColor: '#D5A6BD'}}
                                onPress={() => navigate('Chemin', {
                                    src: getSrc(),
                                    dst: getDst(),
                                    path: paths[selectedPathIndex]
                                })}>
                                <Text>
                                    GO
                                </Text>
                            </Button>
                        </CardItem>
                    </Card>
                </React.Fragment>
            ) : <Spinner color="#D5A6BD"/>
            }
        </Container>
    );

};

export default Propositions;
