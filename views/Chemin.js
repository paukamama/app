import React from 'react';
import {Card, CardItem, Container, Content} from 'native-base';
import percentageToHsl from '../modules/percentageToHsl'
import PropositionMap from '../components/Propositions/PropositionMap'
import PathInfoCard from '../components/Chemin/PathInfoCard'

const Chemin = (props) => {

    const getSrc = () => props.navigation.getParam('src');

    const getDst = () => props.navigation.getParam('dst');

    const getColorFromPollution = (pollution, index) => {
        if (pollution >= 0 && pollution <= 100)
            return percentageToHsl(pollution);
        return "#7b7b7b";
    };

    const getLineWeight = (index) => 6;

    const path = props.navigation.getParam('path');

    return (
        <Container>
            <Content>
                <PropositionMap
                    paths={[path]}
                    getColorFromPollution={getColorFromPollution}
                    getLineWeight={getLineWeight}
                    setSelectedPathIndex={() => {}}
                    src={getSrc()}
                    dst={getDst()} 
                />
            </Content>
            <Card transparent>
                <CardItem>
                    <PathInfoCard path={path}/>
                </CardItem>
            </Card>
        </Container>
    );

};

export default Chemin;
