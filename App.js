import React from 'react';

//import { StyleSheet, Text, View } from 'react-native'; //old
//import Search from './components/Search' //old

import { Container, Header, Content, Form, Item, Input, Button, Text } from 'native-base';
import * as Font from 'expo-font';
import Navigation from './navigation/Navigation'


export default class App extends React.Component {

	state = {
		loading: true
	}
	
	async componentDidMount(){
		await Font.loadAsync({
			'Roboto': require('native-base/Fonts/Roboto.ttf'),
			'Roboto_medium': require('native-base/Fonts/Roboto_medium.ttf')
		})
		this.setState({ loading: false })
	}
	
	render() {
		if (this.state.loading) {
			return (
				<Container></Container>
			);
		}
		return (
			<Navigation/>
		)
	}
}




