import Addr from '../modules/Addr'
import Route from '../modules/Route'

const savedRoutesDB = [
    new Route(
        new Addr(
            "Institut National des Sciences Appliquées de Lyon, 20, Avenue Albert Einstein, Croix-Luizet, Villeurbanne",
            {
                "latitude": 45.7830905,
                "longitude": 4.8759893530497695
            }),
        new Addr(
            "Place Bellecour, Rue Simon Maupin, Bellecour, Lyon 2e Arrondissement, Lyon",
            {
                "latitude": 45.7575158,
                "longitude": 4.831995242486349
            })
    ),
    new Route(
        new Addr(
            "La Part-Dieu, Rue Servient, Part-Dieu, Lyon 3e Arrondissement, Lyon",
            {
                "latitude": 45.76143544999999,
                "longitude": 4.856453413547852
            }),
        new Addr(
            "Fourvière, Place de Fourvière, Saint-Georges, Fourvière, Lyon 5e Arrondissement, Lyon",
            {
                "latitude": 45.762381,
                "longitude": 4.8214507
            })
    )
];

export default savedRoutesDB;