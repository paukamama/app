import fetchJson from '../modules/fetchJson'
import Addr from '../modules/Addr'

const addrFromNominatimResult = (nominatimData) => {
    const county = nominatimData["address"]["county"];
    let displayName = nominatimData["display_name"];
    const indexOfCounty = displayName.lastIndexOf(`, ${county},`);
    if(indexOfCounty>0)
        displayName = displayName.substring(0, indexOfCounty);
    const coord = {
        "latitude": parseFloat(nominatimData["lat"]),
        "longitude": parseFloat(nominatimData["lon"])
    };
    return new Addr(displayName, coord);
};

const handleAddressLookup = (addr) => {
    const url = addr.split(' ').join('+');
    return fetchJson(`http://nominatim.openstreetmap.org/search?q=${url}&format=json&countrycodes=fr&limit=2&viewbox=45.454026,4.2436423,46.3064984,5.1603205&addressdetails=1`)
        .then(data => {
            let suggestions = [];
            data.map(s => suggestions.push(addrFromNominatimResult(s)));
            return suggestions;
        });
}

export default handleAddressLookup;