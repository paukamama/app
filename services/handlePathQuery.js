import fetchJson from '../modules/fetchJson'

const handlePathQuery = (src,dst) => {
    return fetchJson(`http://oxygo.westeurope.cloudapp.azure.com/route/src/${src["latitude"]}/${src["longitude"]}/dst/${dst["latitude"]}/${dst["longitude"]}/mobile/`)
};

export default handlePathQuery;