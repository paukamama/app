import {createStackNavigator} from 'react-navigation-stack'
import {createAppContainer} from 'react-navigation'
import Accueil from '../views/Accueil'
import Propositions from '../views/Propositions'
import Chemin from '../views/Chemin'
import React from 'react';
import {Image} from "react-native";

const Logo = () => <Image style={{height: 45, width: 80}} source={require('../assets/logo_full_white_grey.png')}/>;

const navigationOptions = {
    headerStyle: {
        backgroundColor: '#D5A6BD',
    },
    headerTitle: () => <Logo/>,
    headerTitleAlign: "center"
};

const AccueilStackNavigator = createStackNavigator({
    Accueil: {
        screen: Accueil,
        navigationOptions: navigationOptions
    },
    Propositions: {
        screen: Propositions,
        navigationOptions: navigationOptions
    },
    Chemin: {
        screen: Chemin,
        navigationOptions: navigationOptions
    }
});

export default createAppContainer(AccueilStackNavigator)
