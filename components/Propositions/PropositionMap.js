import React from 'react';
import {Dimensions, StyleSheet, View} from 'react-native'
import MapView, {Marker, Polyline} from 'react-native-maps'
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome'
import {faDotCircle, faMapMarkerAlt} from '@fortawesome/free-solid-svg-icons'

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    mapStyle: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
    }
});


class PropositionMap extends React.Component {
    fitMap = () => {
        const {src, dst} = this.props;
        this.map.fitToCoordinates([src, dst], {
            edgePadding: {top: 100, right: 100, bottom: 400, left: 100},
            animated: false,
        });
    }

    render(){
        const {paths, getColorFromPollution, getLineWeight, setSelectedPathIndex, src, dst} = this.props;
        return (
            <View style={styles.container}>
                <MapView onLayout={() => this.fitMap()} ref={(ref) => {this.map = ref}} style={styles.mapStyle}>
                    {paths.map((path, index) => {
                        return (
                            <React.Fragment key={index}>
                                {path.subpaths.map((subpath, subindex) => (
                                    <React.Fragment key={subindex}>
                                        <Polyline coordinates={subpath.coordinates}
                                                    strokeColor={getColorFromPollution(subpath.pollution, index)}
                                                    strokeWidth={getLineWeight(index)}
                                                    tappable={true}
                                                    onPress={()=>setSelectedPathIndex(index)}/>
                                    </React.Fragment>
                                ))}
                            </React.Fragment>
                        );
                    })}
                    <Marker coordinate={src}>
                        <FontAwesomeIcon icon={faDotCircle} color={'#434343'} size={18}/>
                    </Marker>
                    <Marker coordinate={dst}>
                        <FontAwesomeIcon icon={faMapMarkerAlt} color={'#434343'} size={30}/>
                    </Marker>
                </MapView>
            </View>
        );
    }

}

export default PropositionMap;