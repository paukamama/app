import React from 'react';
import {StyleSheet} from 'react-native'
import {Button, Col, Grid, Text} from 'native-base';

const styles = StyleSheet.create({
    activeButton: {
        backgroundColor: '#D5A6BD'
    },
    inactiveButton: {
        backgroundColor: '#7E7E7E'
    }
});

const PathSwitcher = (props) => {
    const {isFastestPathSelected, isCleanestPathSelected, setSelectedPathIndex, fastestPathIndex, cleanestPathIndex} = props;

    return (
        <Grid>
            <Col>
                <Button
                    block
                    style={(isCleanestPathSelected) ? styles.activeButton : styles.inactiveButton}
                    onPress={()=>setSelectedPathIndex(cleanestPathIndex)}
                >
                    <Text>Le moins pollué</Text>
                </Button>
            </Col>
            <Col>
                <Button
                    block
                    style={(isFastestPathSelected) ? styles.activeButton : styles.inactiveButton}
                    onPress={()=>setSelectedPathIndex(fastestPathIndex)}
                >
                    <Text>Le plus rapide</Text>
                </Button>
            </Col>
        </Grid>
    );
}

export default PathSwitcher;