import React from 'react';
import PropTypes from "prop-types";
import {Col, Grid, Text} from 'native-base';
import {faBicycle, faClock, faSmog} from '@fortawesome/free-solid-svg-icons'
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome'

const PathInfoCard = (props) => {
    const {path, color, mutedColor} = props;
    return (
        <Grid>
            <Col>
                <Text style={{color: color, fontWeight: "bold"}}>
                    <FontAwesomeIcon color={color} icon={faSmog}/> {path["pollution"]}/100
                </Text>
            </Col>
            <Col>
                <Text style={{color: mutedColor}}>
                    <FontAwesomeIcon color={mutedColor} icon={faClock}/> {path["duration"]} min
                </Text>
            </Col>
            <Col>
                <Text style={{color: mutedColor}}>
                    <FontAwesomeIcon color={mutedColor} icon={faBicycle}/> {path["distance"]} km
                </Text>
            </Col>
        </Grid>
    );
}

export default PathInfoCard;