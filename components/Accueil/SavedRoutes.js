import React from 'react';
import {
    Button,
    Card,
    CardItem,
    Col,
    Container,
    DeckSwiper,
    Grid,
    Header,
    Row,
    Text,
    Title
} from 'native-base';
import {faArrowLeft, faArrowRight} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome'
import {StyleSheet, TouchableOpacity} from "react-native";

const styles = StyleSheet.create({
    center: {
        flexDirection: "column",
        justifyContent: "center",
        backgroundColor: '#D5A6BD'
    }
});

class SavedRoutes extends React.Component {
    render() {
        const {savedRoutes, navigate} = this.props;
        return (
            <Container style={{padding: 10}}>
                <DeckSwiper
                    ref={(c) => this._deckSwiper = c}
                    looping={true}
                    dataSource={savedRoutes}
                    renderItem={(route) => {
                        return (
                            <Card>
                                <TouchableOpacity onPress={() => navigate('Propositions', {
                                    srcAddress: route.src.coord,
                                    dstAddress: route.dst.coord
                                })}>
                                    <Header style={{
                                        backgroundColor: '#D5A6BD'
                                    }}>
                                        <Grid>
                                            <Row>
                                                <Col size={1} style={styles.center}>
                                                    <Button transparent
                                                            onPress={() => this._deckSwiper._root.swipeLeft()}>
                                                        <FontAwesomeIcon color="white" icon={faArrowLeft}/>
                                                    </Button>
                                                </Col>
                                                <Col size={5} style={styles.center}>
                                                    <Title>Trajets précédents</Title>
                                                </Col>
                                                <Col size={1} style={styles.center}>
                                                    <Button transparent
                                                            onPress={() => this._deckSwiper._root.swipeRight()}>
                                                        <FontAwesomeIcon color="white" icon={faArrowRight}/>
                                                    </Button>
                                                </Col>
                                            </Row>
                                        </Grid>


                                    </Header>

                                    <CardItem header bordered>
                                        <Text style={{color: '#7E7E7E'}}>De</Text>
                                    </CardItem>
                                    <CardItem bordered>
                                        <Text>{route.src.addr}</Text>
                                    </CardItem>
                                    <CardItem footer header bordered>
                                        <Text style={{color: '#7E7E7E'}}>À</Text>
                                    </CardItem>
                                    <CardItem bordered>
                                        <Text>{route.dst.addr}</Text>
                                    </CardItem>
                                </TouchableOpacity>
                            </Card>
                        );
                    }
                    }
                />
            </Container>
        );
    }
}

export default SavedRoutes;