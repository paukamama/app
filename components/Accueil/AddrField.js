import React, {useEffect} from 'react';
import {
    Grid,
    Input,
    List,
    ListItem,
    Row,
    Text
} from 'native-base';

const AddrField = (props) => {
    const {placeholder, value, onChangeText, suggestionsArray, onSuggestionsUpdate} = props;

    return (
        <Grid>
            <Row>
                <Input onChangeText={addr => onChangeText(addr)}
                        placeholder={placeholder} value={value}/>
            </Row>
            <Row>
                <List dataArray={suggestionsArray}
                        renderRow={(addr) => {
                            return (
                                <ListItem onPress={() => onSuggestionsUpdate(addr)}>
                                    <Text>{addr.addr}</Text>
                                </ListItem>
                            );
                        }
                        }
                />
            </Row>
        </Grid>
    );
}

export default AddrField;