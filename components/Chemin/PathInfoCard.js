import React from 'react';
import PropTypes from "prop-types";
import {Row, Col, Grid, Text} from 'native-base';
import {faBicycle, faClock} from '@fortawesome/free-solid-svg-icons'
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome'

const PathInfoCard = (props) => {
    const {path} = props;
    return (
        <Grid style={{paddingBottom: 20}}>
            <Row>
                <Col>
                    <Text style={{textAlign: 'center'}}>
                        <FontAwesomeIcon icon={faClock}/> {path["duration"]} min
                    </Text>
                </Col>
                <Col>
                    <Text style={{textAlign: 'center'}}>
                        <FontAwesomeIcon icon={faBicycle}/> {path["distance"]} km
                    </Text>
                </Col>
            </Row>
        </Grid>
    );
}

export default PathInfoCard;